export class Vaccine {
    id: string;
    name: string;
    numberOfDoses: number;
    timeBetweenDoses: number;

    constructor() { }
}