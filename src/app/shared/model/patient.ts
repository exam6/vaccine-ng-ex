import { Vaccine } from './vaccine';

export class Patient {
    id: string;
    fullName: string;
    age: number;
    vaccine: string;
    firstDose: boolean;
    secondDose: boolean;
    firstDoseDate: Date;
    secondDoseDate: Date;

    constructor() {
        this.firstDose = false;
        this.secondDose = false;
    }
}
