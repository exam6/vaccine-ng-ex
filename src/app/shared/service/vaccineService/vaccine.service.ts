import { Injectable } from '@angular/core';
import { Vaccine } from '../../model/vaccine';

@Injectable({
  providedIn: 'root'
})
export class VaccineService {

  private PATH = 'vaccineList';

  constructor() { }

  getVaccineList() {
    let vaccineList: Array<Vaccine>;
    if (localStorage.getItem(this.PATH)) {
      vaccineList = JSON.parse(localStorage.getItem(this.PATH));
    } else { vaccineList = []; }
    return vaccineList;
  }

  getVaccineById(id: string) {
    const vaccineList = this.getVaccineList();
    const vaccine = vaccineList.find(item => item.id === id);
    return vaccine;
  }

  registerVaccine(vaccine: Vaccine) {
    let vaccineList = this.getVaccineList();
    if (!this.vaccineExists(vaccine.id)) {
      vaccineList = [vaccine, ...vaccineList];
      this.saveVaccineList(vaccineList);
    }
  }

  deleteVaccine(vaccine: Vaccine) {
    if (this.vaccineExists(vaccine.id)) {
      let list = this.getVaccineList();
      list = list.filter(item => item.id !== vaccine.id);
      this.saveVaccineList(list);
    } else {
      console.log('Vaccine not registered!');
    }
  }

  updateVaccine(vaccine: Vaccine) {
    if (this.vaccineExists(vaccine.id)) {
      const list = this.getVaccineList();
      const index = list.findIndex(v => v.id === vaccine.id);
      list[index] = vaccine;
      this.saveVaccineList(list);
    } else {
      console.log('Vaccine not registered!');
    }
  }

  saveVaccineList(vaccineList: Vaccine[]) {
    localStorage.setItem(this.PATH, JSON.stringify(vaccineList));
  }

  vaccineExists(vaccineId: string) {
    const vaccineList = this.getVaccineList();
    const isRegistered = vaccineList.some(item => item.id === vaccineId);
    return isRegistered;
  }

  subtractDoseOf(vaccineId: string) {
    const vaccine = this.getVaccineById(vaccineId);
    vaccine.numberOfDoses--;
    this.updateVaccine(vaccine);
  }
}
