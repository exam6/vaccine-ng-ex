import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./static-page/static-page.module').then(m => m.StaticPageModule) },
  { path: 'patients', loadChildren: () => import('./patient/patient.module').then(m => m.PatientModule) },
  { path: 'vaccines', loadChildren: () => import('./vaccine/vaccine.module').then(m => m.VaccineModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
