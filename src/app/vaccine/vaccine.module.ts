import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';

import { VaccineRoutingModule } from './vaccine-routing.module';
import { VaccineFormComponent } from './vaccine-form/vaccine-form.component';
import { VaccineListComponent } from './vaccine-list/vaccine-list.component';
import { RegisterVaccineComponent } from './register-vaccine/register-vaccine.component';


@NgModule({
  declarations: [VaccineFormComponent, VaccineListComponent, RegisterVaccineComponent],
  imports: [
    CommonModule,
    VaccineRoutingModule,
    SharedModule
  ]
})
export class VaccineModule { }
