import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Vaccine } from '../../shared/model/vaccine';

@Component({
  selector: 'app-vaccine-form',
  templateUrl: './vaccine-form.component.html',
  styleUrls: ['./vaccine-form.component.scss']
})
export class VaccineFormComponent implements OnInit {

  form: FormGroup;
  @Input() vaccine: Vaccine;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onSubmit: EventEmitter<any>;

  constructor() {
    this.onSubmit = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.vaccine = new Vaccine();
  }

  save() {
    this.onSubmit.emit(this.vaccine);
  }

}
