import { Component, OnInit, ViewChild } from '@angular/core';

import { VaccineService } from 'src/app/shared/service/vaccineService/vaccine.service';
import { Vaccine } from '../../shared/model/vaccine';
import { Router } from '@angular/router';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-vaccine-list',
  templateUrl: './vaccine-list.component.html',
  styleUrls: ['./vaccine-list.component.scss']
})
export class VaccineListComponent implements OnInit {

  vaccineList: Vaccine[];
  selectedVaccine: Vaccine;
  modalConfig = {
    modalTitle: 'Paciente',
    closeButtonLabel: 'Cerrar',
    hideDismissButton: true
  };

  @ViewChild('modal') private modalComponent: ModalComponent;
  constructor(private vaccineService: VaccineService, private router: Router) { }

  ngOnInit(): void {
    this.getVaccineList();
  }

  getVaccineList() {
    this.vaccineList = this.vaccineService.getVaccineList();
  }

  async viewVaccine(vaccine: Vaccine) {
    this.selectedVaccine = this.vaccineService.getVaccineById(vaccine.id);
    return await this.modalComponent.open();
  }

  addNew() {
    this.router.navigateByUrl('/vaccines/register-vaccine');
  }

}
