import { Component, OnInit } from '@angular/core';

import { Vaccine } from '../../shared/model/vaccine';
import { VaccineService } from '../../shared/service/vaccineService/vaccine.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-vaccine',
  templateUrl: './register-vaccine.component.html',
  styleUrls: ['./register-vaccine.component.scss']
})
export class RegisterVaccineComponent implements OnInit {

  constructor(private vaccineService: VaccineService, private router: Router) { }

  ngOnInit(): void {
  }

  registerVaccine(vaccine: Vaccine) {
    this.vaccineService.registerVaccine(vaccine);
    this.router.navigateByUrl('/vaccines/vaccine-list');
  }
}
