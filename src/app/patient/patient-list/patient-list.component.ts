import { Component, OnInit, ViewChild } from '@angular/core';

import { PatientService } from 'src/app/shared/service/patientService/patient.service';
import { Patient } from '../../shared/model/patient';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})

export class PatientListComponent implements OnInit {

  patientList: Patient[];
  searchId = '';
  selectedPatient: Patient;
  secondDoseDate: Date;
  modalConfig = {
    modalTitle: 'Paciente',
    closeButtonLabel: 'Cerrar',
    hideDismissButton: true
  };

  @ViewChild('modal') private modalComponent: ModalComponent;
  constructor(private patientService: PatientService) { }

  ngOnInit(): void {
    this.getPatientList();
  }

  getPatientList() {
    this.patientList = this.patientService.getPatientList();
  }

  registerDose(patient: Patient, dose: number) {
    const date = new Date();
    const vaccine = this.patientService.getPatientVaccine(patient.id);
    if (dose === 1) {
      this.patientService.setFirstDoseDate(patient, date);
      this.secondDoseDate = this.patientService.calculateSecondDoseDate(date, vaccine.timeBetweenDoses);
    } else {
      this.patientService.setSecondDoseDate(patient, date);
    }
  }

  async viewPatient(patientId: string) {
    this.selectedPatient = this.patientService.getPatientById(patientId);
    return await this.modalComponent.open();
  }
}
